use std::collections::HashMap;

fn main() {
    let input = include_str!("input.txt");
    let coords: Vec<(i32, i32)> = input.lines().map(|l| {
        let lr: Vec<&str> = l.split(", ").collect();
        (lr[0].parse::<i32>().unwrap(), lr[1].parse::<i32>().unwrap())
    }).collect();

    let mut cnts = HashMap::new();
    let mut infs = HashMap::new();

    let minx = coords.iter().map(|i| i.0).min().unwrap();
    let miny = coords.iter().map(|i| i.1).min().unwrap();
    let maxx = coords.iter().map(|i| i.0).max().unwrap();
    let maxy = coords.iter().map(|i| i.1).max().unwrap();

    let mut close: Vec<(i32, i32)> = vec![];

    for x in minx..=maxx {
        for y in miny..=maxy {
            let mut closest = i32::max_value();
            for (ind, coord) in coords.iter().enumerate() {
                let dist = (coord.0 - x).abs() + (coord.1 - y).abs();
                if dist < closest {
                    closest = dist;
                    close.drain(..);
                }
                if dist == closest {
                    close.push((dist, ind as i32))
                }
            }
            if close.len() == 1 {
                if x == minx || y == miny || x == maxx || y == maxy {
                    infs.insert(close[0].1, true);
                }
                let cnt = cnts.entry(close[0].1).or_insert(0);
                *cnt += 1;
            }
        }
    }

    let max = cnts.iter().filter(|i| !infs.contains_key(i.0))
        .max_by_key(|i| i.1);
    println!("done: {:?}", max);
}
