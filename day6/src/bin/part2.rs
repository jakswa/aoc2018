fn main() {
    let input = include_str!("input.txt");
    let coords: Vec<(i32, i32)> = input.lines().map(|l| {
        let lr: Vec<&str> = l.split(", ").collect();
        (lr[0].parse::<i32>().unwrap(), lr[1].parse::<i32>().unwrap())
    }).collect();

    let minx = coords.iter().map(|i| i.0).min().unwrap();
    let miny = coords.iter().map(|i| i.1).min().unwrap();
    let maxx = coords.iter().map(|i| i.0).max().unwrap();
    let maxy = coords.iter().map(|i| i.1).max().unwrap();

    let mut reg_size = 0;
    for x in 0..=(maxx + 1000) {
        for y in 0..=(maxy + 1000) {
            let mut sum = 0;
            for coord in coords.iter() {
                sum += (coord.0 - x).abs() + (coord.1 - y).abs();
            }
            if sum < 10000 {
                reg_size += 1;
            }
        }
    }
    println!("hmm: {}", reg_size);
}
