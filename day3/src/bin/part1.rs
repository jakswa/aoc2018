extern crate regex;
use regex::Regex;

fn main() {
    let input = include_str!("input.txt");
    let patt = Regex::new("[#@ ,:x]+").unwrap();
    let mut square: [[i32; 1000]; 1000] = [[0; 1000]; 1000];
    input.lines().for_each(|line| {
        let nums = patt.split(line).skip(1)
            .map(|word| word.parse::<i32>().unwrap())
            .collect::<Vec<i32>>();

        let _id = nums[0];
        let x = nums[1];
        let y = nums[2];
        let w = nums[3];
        let h = nums[4];
        for i in 0..h {
            for j in 0..w {
                square[(x + j) as usize][(y + i) as usize] += 1;
            }
        }
    });
    let mut cnt = 0;
    for i in 0..1000 {
        for j in 0..1000 {
            if square[i][j] > 1 {
                cnt += 1;
            }
        }
    }
    println!("CNT {}", cnt);
}
