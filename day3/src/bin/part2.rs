extern crate regex;
use regex::Regex;

fn main() {
    let input = include_str!("input.txt");
    let patt = Regex::new("[#@ ,:x]+").unwrap();
    let mut square: [[i32; 1000]; 1000] = [[0; 1000]; 1000];
    input.lines().for_each(|line| {
        let nums = patt.split(line).skip(1)
            .map(|word| word.parse::<i32>().unwrap())
            .collect::<Vec<i32>>();

        let id = nums[0];
        let x = nums[1];
        let y = nums[2];
        let w = nums[3];
        let h = nums[4];
        for i in 0..h {
            for j in 0..w {
                square[(x + j) as usize][(y + i) as usize] += id;
            }
        }
    });
    input.lines().for_each(|line| {
        let nums = patt.split(line).skip(1)
            .map(|word| word.parse::<i32>().unwrap())
            .collect::<Vec<i32>>();

        let mut valid = true;
        let id = nums[0];
        let x = nums[1];
        let y = nums[2];
        let w = nums[3];
        let h = nums[4];
        for i in 0..h {
            for j in 0..w {
                if square[(x + j) as usize][(y + i) as usize] != id {
                    valid = false
                }
            }
        }
        if valid {
            println!("chosen: {}", id);
        }
    });
}
