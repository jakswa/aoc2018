const SERIAL: i64 = 18; //1309;

fn main() {
    let mut grid: [[i64;300]; 300] = [[0; 300];300];
    for y in 0..300 {
        for x in 0..300 {
            grid[y as usize][x as usize] = pl(x,y);
        }
    }
    let mut tl: (i64, i64, i64) = (0,0,0);
    let mut max: i64 = 0;
    for sq in 1..=300 {
        for y in 1..=(300-sq+1) {
            for x in 1..=(300-sq+1) {
                let val = calc(x,y,sq,&grid);
                if val > max {
                    max = val;
                    tl = (x,y,sq);
                }
            }
        }
    }
    println!("oo: {:?}", tl);
}

fn calc(x: i64, y: i64, size: i64, grid: &[[i64;300];300]) -> i64 {
    let mut sum: i64 = 0;
    for yy in 0..size {
        let y = y + yy - 1;
        for xy in 0..size {
            let x = x + xy - 1;
            sum += grid[y as usize][x as usize];
        }
    }
    sum
}

fn pl(x: i64, y: i64) -> i64 {
    let rid = x + 10;
    (rid * y + SERIAL) * rid / 100 % 10 - 5
}
