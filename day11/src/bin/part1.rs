const SERIAL: i64 = 1309;

fn main() {
    let mut tl: (i64, i64) = (0,0);
    let mut max: i64 = 0;
    for y in 1..=300 {
        for x in 1..=300 {
            let val = calc(x,y);
            if val > max {
                max = val;
                tl = (x,y);
            }
        }
    }
    println!("oo: {:?}", tl);
}

fn calc(x: i64, y: i64) -> i64 {
    let mut sum: i64 = 0;
    for yy in 0..3 {
        for xy in 0..3 {
            sum += pl(x + xy, y + yy);
        }
    }
    sum
}

fn pl(x: i64, y: i64) -> i64 {
    let rid = x + 10;
    (rid * y + SERIAL) * rid / 100 % 10 - 5
}
