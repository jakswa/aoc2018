fn main() {
    let input: Vec<u8> = include_str!("input.txt")
        .split_whitespace().map(|i| i.parse::<u8>().unwrap())
        .collect();

    let res = dig(input[0], input[1], &input[2..]);
    println!("end: {:?}", res);
}

fn dig(childs: u8, metas: u8, slice: &[u8]) -> (u32, u32) {
    // println!("dig: {:?}, {:?} -- {:?}...", childs, metas, &slice[0..3]);
    let mut pos: u32 = 0;
    let mut sum: u32 = 0;
    let mut child_vals: Vec<u32> = vec![];
    if childs > 0 {
        for _i in 0..childs {
            let rest = (pos + 2) as usize;
            let sumlen = dig(slice[(pos + 0) as usize], slice[(pos + 1) as usize], &slice[rest..]);
            pos += sumlen.1;
            child_vals.push(sumlen.0);
            // println!("({:?}) -- sumlen: {:?}, sum: {:?}, pos: {:?}", childs, sumlen, sum, pos);
        }
    }
    if metas > 0 {
        if childs == 0 {
            for _i in 0..metas {
                sum += slice[pos as usize] as u32;
                pos += 1;
            }
        } else {
            //println!("CHILDREN: {:?}", (child_vals, slice, pos, metas));
            for _i in 0..metas {
                let ind = slice[pos as usize];
                if ind > 0 && child_vals.len() >= ind as usize {
                    sum += child_vals[(ind - 1) as usize];
                }
                pos += 1;
            }
        }
        // println!(" -->(metas++) sum: {:?}, pos: {:?}", sum, pos);
    }
    (sum, pos + 2)
}
