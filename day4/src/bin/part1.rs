extern crate regex;
use regex::Regex;

use std::collections::HashMap;

fn main() {
    let input = include_str!("input.txt");

    let time_pat = Regex::new(r"[-\d :]+").unwrap();
    let id_pat = Regex::new(r"#([\d]+)\b").unwrap();
    let min_pat = Regex::new(r"(\d\d)] [wf]").unwrap();

    let mut lines: Vec<&str> = input.lines().collect();
    lines.sort_unstable_by_key(|i| {
        time_pat.find(i).unwrap().as_str()
    });

    let mut guards: Vec<Vec<u32>> = vec![];
    let mut guard: Vec<u32> = vec![];
    let mut sums = HashMap::new();

    // parse guard shifts into Vec<Vec<u32>>,
    // one Vec<u32> for each guard
    lines.iter().for_each(|line| {
        if let Some(min) = min_pat.captures(line) {
            let minu32 = min.get(1).unwrap().as_str().parse::<u32>().unwrap();
            guard.push(minu32);
        }
        if let Some(id_val) = id_pat.captures(line) {
            let idu32 = id_val.get(1).unwrap().as_str().parse::<u32>().unwrap();
            if guard.len() > 0 {
                guards.push(guard.drain(..).collect());
            }
            guard.push(idu32);
        }
    });

    guards.iter().for_each(|g| {
        let sum = g[1..].chunks(2).fold(0, |acc, n| {
            acc + (n[1] - n[0])
        });
        let val = sums.entry(g[0]).or_insert(0);
        *val += sum;
    });

    let max_guard_id = *sums.iter().max_by_key(|k| k.1).unwrap().0;
    let max_sched: Vec<Vec<u32>> = guards.iter().cloned().filter(|g| g[0] == max_guard_id).collect();
    let mut minutes: [u32; 100] = [0; 100];
    max_sched.iter().for_each(|g| {
        g[1..].chunks(2).for_each(|i| {
            (i[0]..i[1]).for_each(|n| minutes[n as usize] += 1);
        });
    });
    let wat = minutes.iter_mut().enumerate().max_by_key(|k| *k.1).unwrap().0;
    println!("max: {:?}:{:?}", max_guard_id, wat);
}
