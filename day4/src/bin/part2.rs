extern crate regex;
use regex::Regex;

use std::collections::HashMap;

fn main() {
    let input = include_str!("input.txt");

    let time_pat = Regex::new(r"[-\d :]+").unwrap();
    let id_pat = Regex::new(r"#([\d]+)\b").unwrap();
    let min_pat = Regex::new(r"(\d\d)] [wf]").unwrap();

    let mut lines: Vec<&str> = input.lines().collect();
    lines.sort_unstable_by_key(|i| {
        time_pat.find(i).unwrap().as_str()
    });

    let mut guard: Vec<u32> = vec![];
    let mut minutes = HashMap::new();

    // parse guard shifts into Vec<Vec<u32>>,
    // one Vec<u32> for each guard
    lines.iter().for_each(|line| {
        if let Some(min) = min_pat.captures(line) {
            let minu32 = min.get(1).unwrap().as_str().parse::<u32>().unwrap();
            guard.push(minu32);
        }
        if let Some(id_val) = id_pat.captures(line) {
            let idu32 = id_val.get(1).unwrap().as_str().parse::<u32>().unwrap();
            if guard.len() > 0 {
                let val = minutes.entry(guard[0] as usize).or_insert([0; 60]);
                guard[1..].chunks(2).for_each(|i| {
                    (i[0]..i[1]).for_each(|n| val[n as usize] += 1);
                });
                guard.drain(..);
            }
            guard.push(idu32);
        }
    });

    let max = minutes.iter().max_by_key(|mins| {
        mins.1.iter().max()
    }).unwrap();

    println!("oof: {}, {}", max.0, max.0 * max.1.iter().enumerate().max_by_key(|n| n.1).unwrap().0);
}
