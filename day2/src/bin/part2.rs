extern crate itertools;

use std::fs::File;
use std::io::prelude::*;
use itertools::Itertools;

fn main() {
    let filename = "input.txt".to_string();
    find_box(&filename);
}

fn find_box(filename: &String) {
    let mut f = File::open(filename).expect("file not found");

    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the file");

    let lines = contents.split_whitespace();
    let res = lines.combinations(2).find(|x| compare(&x[0], &x[1])).expect("none found");
    println!("result {}, {}", res[0], res[1]);
}

fn compare(word: &str, word2: &str) -> bool {
    let mut diff = 0;
    for i in word.chars().zip(word2.chars()) {
        match i.0 != i.1 {
            false => (),
            true => diff += 1
        }
    }
    diff == 1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn count_works() {
    }
}
