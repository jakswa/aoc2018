use std::fs::File;
use std::io::prelude::*;
use std::collections::HashMap;

fn main() {
    println!("checksum: {}", checkminus(&"input.txt".to_string()));
}

fn checkminus(filename: &String) -> i32 {
    let mut f = File::open(filename).expect("file not found");

    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the file");

    let lines = contents.split_whitespace();

    let mut twos = 0;
    let mut threes = 0;

    lines.for_each(|line| {
        match count(line) {
            (true, false) => twos += 1,
            (false, true) => threes += 1,
            (true, true) => { twos += 1; threes += 1; },
            _ => ()
        }
    });
    twos * threes
}

fn count(word: &str) -> (bool, bool) {
    let mut cnts = HashMap::new();
    for i in word.chars() {
        let cnt = cnts.entry(i).or_insert(0);
        *cnt += 1
    }
    let twos = match cnts.values().find(|&&x| x == 2) {
        None => false,
        _ => true
    };
    let threes = match cnts.values().find(|&&x| x == 3) {
        None => false,
        _ => true
    };
    (twos, threes)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn count_works() {
        assert_eq!(count(&"wat".to_string()), (false, false));
        assert_eq!(count(&"wattt".to_string()), (false, true));
        assert_eq!(count(&"waatt".to_string()), (true, false));
        assert_eq!(count(&"waattt".to_string()), (true, true));
    }

    #[test]
    fn checkminus_works() {
        assert_eq!(checkminus(&"test.txt".to_string()), 12)
    }
}
