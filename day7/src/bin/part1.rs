use std::collections::HashMap;

fn main() {
    let input = include_str!("input.txt");
    let mut tree: HashMap<u8, Vec<u8>> = HashMap::new();
    let mut ran: HashMap<u8, bool> = HashMap::new();

    let mut steps: Vec<u8> = vec![];
    let mut lines: Vec<&str> = input.lines().collect();
    lines.sort_unstable();

    let mut curr: u8 = lines.iter().next().unwrap().as_bytes()[5];

    lines.iter().for_each(|l| {
        let bytes = l.as_bytes();
        if bytes[5] != curr {
            tree.insert(curr, steps.drain(..).collect());
            curr = bytes[5];
        }
        steps.push(bytes[36]);
    });
    tree.insert(curr, steps.drain(..).collect());

    let mut steps_in_order: Vec<u8> = vec![];
    let mut to_run = tree.keys().map(|i| *i).collect::<Vec<u8>>();
    tree.values().flatten().for_each(|i| to_run.push(*i));
    to_run.sort_unstable();
    to_run.dedup();

    loop {
        let mut steps = to_run.iter().filter(|i| !ran.contains_key(i) && !tree.values().flatten().any(|j| *i == j)).map(|i| *i).collect::<Vec<u8>>();
        steps.sort_unstable();
        if steps.len() == 0 { break }

        let curr = steps.remove(0);
        steps_in_order.push(curr);
        tree.remove(&curr);
        ran.insert(curr, true);
    }

    println!("done! {:?}", std::str::from_utf8(&steps_in_order).unwrap());
}
