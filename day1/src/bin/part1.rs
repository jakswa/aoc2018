use std::fs::File;
use std::io::prelude::*;

pub fn main() {
    let mut f = File::open("input.txt").expect("file not found");

    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the file");

    let lines = contents.split_whitespace();

    let cnt = lines.fold(0, |acc, x| acc + x.parse::<i32>().unwrap() );
    println!("wee {}", cnt)
}
