use std::fs::File;
use std::process::exit;
use std::io::prelude::*;
use std::collections::HashMap;

pub fn main() {
    let mut f = File::open("input.txt").expect("file not found");

    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the file");

    let lines = contents.split_whitespace().map(|x| x.parse::<i32>().unwrap() );

    let mut past = HashMap::new();
    let mut freq = 0;

    loop {
        for i in lines.clone().by_ref() {
            freq = freq + i;
            if past.contains_key(&freq) {
                println!("found: {}", freq);
                exit(0)
            }
            past.insert(freq, true);
        }
    }
}
