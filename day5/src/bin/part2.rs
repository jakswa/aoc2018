use std::collections::HashMap;

fn main() {
    let input = include_str!("input.txt");
    let mut orig = input.as_bytes().to_vec();
    orig.pop(); // popping off newline char

    let mut lens = HashMap::new();

    // 'A' - 'Z'
    for chr in 65..91 {
        let mut chars: Vec<&u8> = orig.iter().filter(|i| **i != chr && **i != chr + 32).collect();
        let mut new_chars: Vec<&u8> = vec![];
        let mut i = 0;
        loop {
            if i == chars.len() - 1 {
                new_chars.push(chars[i]);
                i += 1;
            }
            if new_chars.len() == chars.len() {
                break;
            }
            if i >= chars.len() {
                i = 0;
                chars = new_chars;
                new_chars = vec![];
            } 
            let diff = (*chars[i] as i8) - (*chars[i + 1] as i8);
            if diff.abs() == 32 {
                // println!("diff: {} - {}", chars[i], chars[i+1]);
                i += 2;
                continue;
            }

            new_chars.push(chars[i]);
            i += 1;
        }
        lens.insert(chr, chars.len());
    }
    println!("len: {}", lens.values().min_by_key(|k| *k).unwrap());
}
