fn main() {
    let input = include_str!("input.txt");
    let mut chars = input.as_bytes().to_vec();
    let mut new_chars: Vec<u8> = vec![];
    let mut i = 0;
    println!("start len: {}, last: {}", chars.len(), chars.last().unwrap());
    chars.pop(); // popping off newline char
    loop {
        if i == chars.len() - 1 {
            new_chars.push(chars[i]);
            i += 1;
        }
        if new_chars.len() == chars.len() {
            break;
        }
        if i >= chars.len() {
            i = 0;
            chars = new_chars;
            new_chars = vec![];
        } 
        let diff = (chars[i] as i8) - (chars[i + 1] as i8);
        if diff.abs() == 32 {
            // println!("diff: {} - {}", chars[i], chars[i+1]);
            i += 2;
            continue;
        }

        new_chars.push(chars[i]);
        i += 1;
    }
    println!("len: {}", chars.len());
}
