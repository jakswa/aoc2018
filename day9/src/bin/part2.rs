use std::collections::VecDeque;
const PLAYER_CNT: u32 = 468;
const LAST_MARB: u32 = 71843 * 100;

fn main() {
    let mut players: [u32; PLAYER_CNT as usize] = [0; PLAYER_CNT as usize];

    let mut circle: VecDeque<u32> = VecDeque::with_capacity(LAST_MARB as usize);
    circle.push_back(0);

    for marb in 1..=LAST_MARB {
        // println!("iter: {:?}, {:?}", marb, circle);
        if marb % 23 == 0 {
            let player = (marb % PLAYER_CNT) as usize;
            players[player] += marb;
            (0..7).for_each(|_i| {
                let val = circle.pop_back().unwrap();
                circle.push_front(val);
            });
            players[player] += circle.pop_back().unwrap();
            let val = circle.pop_front().unwrap();
            circle.push_back(val);
            continue;
        }
        let val = circle.pop_front().unwrap();
        circle.push_back(val);
        circle.push_back(marb);
    }
    // println!("iter: {:?}", circle);
    println!("done: {:?}", players.iter().max().unwrap());
}
