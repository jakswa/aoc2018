const PLAYER_CNT: u32 = 468;
const LAST_MARB: u32 = 71843;

fn main() {
    let mut players: [u32; PLAYER_CNT as usize] = [0; PLAYER_CNT as usize];

    let mut circle: Vec<u32> = vec![0];

    let mut index: i32 = 0;
    for marb in 1..=LAST_MARB {
        if marb % 23 == 0 {
            let player = (marb % PLAYER_CNT) as usize;
            players[player] += marb;
            index = index - 7;
            if index < 0 { index = circle.len() as i32 + index }
            players[player] += circle.remove(index as usize);
            continue;
        }
        index = (index + 2) % circle.len() as i32;
        if index == 0 {
            index = circle.len() as i32;
        }
        circle.insert(index as usize, marb);
        // println!("{:?}", circle.clone());
    }
    println!("done: {:?}", players.iter().max().unwrap());
}
