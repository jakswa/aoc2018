extern crate regex;

use regex::Regex;
use std::io::{self, Write};

fn main() {
    let regex = Regex::new(r"-?[0-9]+").unwrap();
    let input: &str = include_str!("input.txt");
    let mut coords: Vec<i32> = regex.find_iter(input).map(|j| {
        let str = j.as_str().trim();
        str.parse::<i32>().unwrap()
    }).collect();

    let mut sq_start = i32::max_value();
    let mut cnt: u32 = 0;
    loop {
        cnt += 1;
        let mut max_min = (
            i32::min_value(), i32::min_value(),
            i32::max_value(), i32::max_value()
        );
        for i in (0..coords.len()).step_by(4) {
            coords[i] += coords[i + 2];
            coords[i + 1] += coords[i + 3];
            // check max X/Y
            if coords[i] > max_min.0 {
                max_min.0 = coords[i];
            }
            if coords[i + 1] > max_min.1 {
                max_min.1 = coords[i + 1];
            }
            // check min X
            if coords[i] < max_min.2 {
                max_min.2 = coords[i];
            }
            if coords[i + 1] < max_min.3 {
                max_min.3 = coords[i + 1];
            }
        }
        if cnt > 10629 {
            for i in 150..180 {
                for j in 0..100 {
                    let needle = (0..coords.len()).into_iter().step_by(4).find(|k| {
                        coords[k + 0] == j + 100 && coords[k + 1] == i
                    });
                    match needle {
                        Some(_o) => io::stdout().write(b"X").unwrap(),
                        None => io::stdout().write(b" ").unwrap()
                    };
                }
                io::stdout().write(b"\n").unwrap();
            }
        }
        let dist = (max_min.1 - max_min.3) + (max_min.0 - max_min.2);
        if dist > sq_start {
            println!("minmax: {:?}", max_min);
            break;
        }
        sq_start = dist;
    }
    println!("done in {:?}, sq_start={:?}", cnt, sq_start);
}
